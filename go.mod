module gitlab.com/tslocum/cview

go 1.12

require (
	github.com/gdamore/tcell/v2 v2.1.0
	github.com/lucasb-eyer/go-colorful v1.0.3
	github.com/mattn/go-runewidth v0.0.9
	github.com/rivo/uniseg v0.2.0
	gitlab.com/tslocum/cbind v0.1.4
	golang.org/x/sys v0.0.0-20201223074533-0d417f636930 // indirect
	golang.org/x/text v0.3.4 // indirect
)
